/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <log/log.h>
#include "dssd.h"

#define FILE_PATH_DIR			"/data/vendor/ramdump"
#define FILE_PATH_RAMDUMP_DIR		FILE_PATH_DIR
#define FILE_PATH_RAMDUMP		FILE_PATH_RAMDUMP_DIR "/"
#define PATH_RAMDUMP_BLK		"/dev/block/by-name/ramdump"


#define RAMDUMP_SAVED_MAGIC		(0xCAFEBABA)
#define METADATA_OFFSET			(0)
#define METADATA_SIZE			(512 * 8)
#define RAMDUMP_OFFSET			METADATA_SIZE
#define DRAM_SIZE_2G			(0x80000000ULL)
#define DRAM_BASE			DRAM_SIZE_2G
#define DRAM_BASE2			(0x880000000ULL)
#define FILE_WRITE_UNIT			(1 * 1024 * 1024)
#define STORE_RAMDUMP_UNIT		(0x40000000ULL)
#define MAX_32bit_VALUE			(0xFFFFFFFFULL)


#pragma pack(push, 1)
union store_ramdump_metadata {
	struct _metadata {
		unsigned int magic;
		unsigned int flag_data_to_storage;
		unsigned long long dram_size;
		unsigned long long dram_start_addr;
		char file_name[512];
	} data;
	char reserved[METADATA_SIZE];
};
#pragma pack(pop)

union store_ramdump_metadata metadata;
void * file_buf;

void file_data_read(FILE *fd, size_t offset, size_t size, void *buf)
{
	size_t temp;
	size_t index = 0;

	fseek(fd, offset, SEEK_SET);

	do {
		temp = fread((void *)((unsigned long long)buf + index), 1, size - index, fd);
		index += temp;
	} while (index < size);
}

void file_data_write(FILE *fd, size_t offset, size_t size, void *buf)
{
	size_t temp;
	size_t index = 0;

	fseek(fd, offset, SEEK_SET);

	do {
		temp = fwrite((void *)((unsigned long long)buf + index), 1, size - index, fd);
		index += temp;
	} while (index < size);
}

void file_copy(FILE *r_fd, FILE *w_fd, size_t r_offset, size_t w_offset, size_t size)
{
	size_t temp = 0;

	do {
		file_data_read(r_fd, r_offset + temp, FILE_WRITE_UNIT, file_buf);
		file_data_write(w_fd, w_offset + temp, FILE_WRITE_UNIT, file_buf);
		temp += FILE_WRITE_UNIT;
	} while (temp < size);
}

int store_ramdump_main(int argc, char **argv) {
	char ramdump_dir[128];
	char ramdump_file_name[512];
	FILE *ramdump_blk_fd;
	FILE *ramdump_file_fd;
	unsigned long long curr_write_size;
	unsigned long long start_addr;
	unsigned long long write_unit_size;
	int fail_cnt_data = 0;
	int fail_cnt_blk = 0;
	int ret;
	int ret_val = 0;

	/* wait storage dir enabled */
	do {
		sleep(5);
		ret = access(FILE_PATH_DIR, F_OK);
		fail_cnt_data++;
		if (fail_cnt_data > 20) {
			ret_val = -ENODEV;
			goto out;
		}
	} while (ret);

	/* wait ramdump blk enabled */
	do {
		sleep(5);
		ret = access(PATH_RAMDUMP_BLK, F_OK);
		fail_cnt_blk++;
		if (fail_cnt_blk > 5) {
			ret_val = -ENODEV;
			goto out;
		}
	} while (ret);

	/* open ramdump blk dev */
	ramdump_blk_fd = fopen(PATH_RAMDUMP_BLK, "rb");
	if (!ramdump_blk_fd) {
		ret_val = -ENODEV;
		goto out;
	}

	/* check ramdump directory */
	ret = access(FILE_PATH_RAMDUMP_DIR, F_OK);
	if (ret) {
		ret = mkdir(FILE_PATH_RAMDUMP_DIR, 0777);
		if (ret) {
			ret_val = -ENODEV;
			goto out_close;
		}
	}

	/* read metadata & check data */
	file_data_read(ramdump_blk_fd, METADATA_OFFSET, METADATA_SIZE, &metadata);
	if (metadata.data.magic != RAMDUMP_SAVED_MAGIC)
		goto out_close;

	/* alloc file buffer */
	file_buf = malloc(FILE_WRITE_UNIT);
	if (!file_buf) {
		ret_val = -ENODEV;
		goto out_close;
	}

	/* make this ramdump directory */
	snprintf(ramdump_dir, sizeof(ramdump_dir),
		FILE_PATH_RAMDUMP "%s", metadata.data.file_name);
	ret = access(ramdump_dir, F_OK);
	if (!ret)
		goto out_free_close;

	ret = mkdir(ramdump_dir, 0775);
	if (ret) {
		ret_val = -ENODEV;
		goto out_free_close;
	}

	/* save ramdump data */
	curr_write_size = 0;
	start_addr = DRAM_BASE;
	do {
		/* set file name & create file */
		if (metadata.data.dram_size - curr_write_size < STORE_RAMDUMP_UNIT)
			write_unit_size = metadata.data.dram_size - curr_write_size;
		else
			write_unit_size = STORE_RAMDUMP_UNIT;

		snprintf(ramdump_file_name, sizeof(ramdump_file_name),
			"%s/ap_0x%llx--0x%llx.lst", ramdump_dir, start_addr,
			start_addr + write_unit_size - 1);
		ramdump_file_fd = fopen(ramdump_file_name, "wb");
		if (!ramdump_file_fd) {
			ret_val = -ENODEV;
			goto out_free_close;
		}

		/* store ramdump file to userdata */
		file_copy(ramdump_blk_fd, ramdump_file_fd,
				RAMDUMP_OFFSET + curr_write_size,
				0, STORE_RAMDUMP_UNIT);
		fflush(ramdump_file_fd);
		fclose(ramdump_file_fd);

		/* set values for next file name */
		curr_write_size += write_unit_size;
		start_addr += write_unit_size;
		if (start_addr > MAX_32bit_VALUE)
			start_addr = DRAM_BASE2 + curr_write_size - DRAM_SIZE_2G;
	} while (curr_write_size < metadata.data.dram_size);

out_free_close:
	free(file_buf);
out_close:
	fclose(ramdump_blk_fd);
out:
	return ret_val;
}
