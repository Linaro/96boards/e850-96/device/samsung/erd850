/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include <cutils/properties.h>
//#define LOG_NDEBUG 0

#define LOG_TAG "ERD850_PowerHAL"
#include <log/log.h>

#include <hardware/hardware.h>
#include <hardware/power.h>

#define SCHEDTUNE_BOOST_PATH		"/dev/stune/top-app/schedtune.boost"
#define SCHEDTUNE_PREFER_PERF_PATH	"/dev/stune/top-app/schedtune.prefer_perf"
#define CLUSTER0_FREQ_PATH		"/dev/cluster0_freq_min"
#define CLUSTER1_FREQ_PATH		"/dev/cluster1_freq_min"
#define MEMORY_FREQ_PATH		"/dev/bus_throughput"

#define SCHEDTUNE_BOOST_NORM		"0"
#define SCHEDTUNE_BOOST_INTERACTIVE	"20"
#define SCHEDTUNE_BOOST_APP_LAUNCH	"40"

#define LAUNCH_BOOST_TIME_NS		(200000000LL)
#define INTERACTION_BOOST_TIME_NS	(1000000000LL)

struct erd850_power_module {
	struct power_module base;
	pthread_mutex_t lock;
	/* EAS schedtune values */
	int schedtune_boost_fd;
	int schedtune_prefer_perf_fd;
	long long deboost_time;
	sem_t signal_lock;
	bool low_power_mode;
};

#define container_of(addr, struct_name, field_name) \
	((struct_name *)((char *)(addr) - offsetof(struct_name, field_name)))

static int sysfs_write(const char *path, char *s) {
	char buf[80];
	int len;
	int fd = open(path, O_WRONLY);

	if (fd < 0) {
		strerror_r(errno, buf, sizeof(buf));
		ALOGE("Error opening %s: %s\n", path, buf);
		return fd;
	}

	len = write(fd, s, strlen(s));
	if (len < 0) {
		strerror_r(errno, buf, sizeof(buf));
		ALOGE("Error writing to %s: %s\n", path, buf);
	}

	close(fd);
	return len;
}

#define NSEC_PER_SEC 1000000000LL
static long long gettime_ns(void) {
	struct timespec ts;

	clock_gettime(CLOCK_MONOTONIC, &ts);
	return ts.tv_sec * NSEC_PER_SEC + ts.tv_nsec;
}

static void nanosleep_ns(long long ns) {
	struct timespec ts;
	ts.tv_sec = ns/NSEC_PER_SEC;
	ts.tv_nsec = ns%NSEC_PER_SEC;
	nanosleep(&ts, NULL);
}

/*[interactive cpufreq gov funcs]*********************************************/
static void interactive_power_init(struct erd850_power_module __unused *erd850) {
	sysfs_write("/sys/power/cpufreq_min_limit", "0");
}

/*[schedtune functions]*******************************************************/

int schedtune_sysfs_boost(struct erd850_power_module *erd850, char* booststr) {
	char buf[80];
	int len;

	if (erd850->schedtune_boost_fd < 0) {
		ALOGE("erd850->schedtune_boost_fd < 0");
		return erd850->schedtune_boost_fd;
	}

	len = write(erd850->schedtune_boost_fd, booststr, 2);
	fsync(erd850->schedtune_boost_fd);

	if (len < 0) {
		strerror_r(errno, buf, sizeof(buf));
		ALOGE("Error writing to %s: %s\n", SCHEDTUNE_BOOST_PATH, buf);
	}
	return 0;
}

int schedtune_sysfs_prefer_perf(struct erd850_power_module *erd850, bool enable) {
	char buf[80];
	int len;

	if (erd850->schedtune_prefer_perf_fd < 0) {
		ALOGE("erd850->schedtune_prefer_perf_fd < 0");
		return erd850->schedtune_prefer_perf_fd;
	}

	if (enable)
		len = write(erd850->schedtune_prefer_perf_fd, "1", 1);
	else
		len = write(erd850->schedtune_prefer_perf_fd, "0", 1);

	fsync(erd850->schedtune_prefer_perf_fd);

	if (len < 0) {
		strerror_r(errno, buf, sizeof(buf));
		ALOGE("Error writing to %s: %s\n", SCHEDTUNE_PREFER_PERF_PATH, buf);
	}
	return 0;
}

int cpu_sysfs_boost(struct erd850_power_module* __unused erd850, unsigned int cpufreq, bool enable) {
	int len;
	char buf[80];

	if (enable) {
		len = snprintf(buf, sizeof(buf) - 1, "%d", cpufreq);
		sysfs_write("/sys/power/cpufreq_min_limit", buf);
	} else {
		sysfs_write("/sys/power/cpufreq_min_limit", "0");
	}

	return 0;
}

/*
int gpu_sysfs_boost(struct erd850_power_module* __unused erd850, unsigned int cpufreq, bool enable) {
	int len;
	char buf[80];

	if (enable) {
		len = snprintf(buf, sizeof(buf) - 1, "%d", cpufreq);
		sysfs_write("/sys/devices/platform/11500000.mali/dvfs_min_lock", buf);
	} else {
		sysfs_write("/sys/devices/platform/11500000.mali/dvfs_min_lock", "0");
	}

	return 0;
}
*/

static int schedtune_boost(struct erd850_power_module *erd850, char* boost_val) {
	int ret;

	ret = schedtune_sysfs_boost(erd850, boost_val);
	if (ret)
		return ret;

	return 0;
}

static int preper_perf_boost(struct erd850_power_module *erd850, int __unused duration) {
	int ret;

	ret = schedtune_sysfs_prefer_perf(erd850, true);
	if (ret)
		return ret;

	return 0;
}

static int cpu_boost(struct erd850_power_module* __unused erd850, unsigned int cpufreq, bool enable) {
	int ret;

	ret = cpu_sysfs_boost(erd850, cpufreq, enable);
	if (ret)
		return ret;

	return 0;
}
/*
static int gpu_boost(struct erd850_power_module* __unused erd850, unsigned int cpufreq, bool enable) {
	int ret;

	ret = gpu_sysfs_boost(erd850, cpufreq, enable);
	if (ret)
		return ret;

	return 0;
}
*/

static void expand_boost_time(struct erd850_power_module *erd850, long long boost_time) {
	long long now = gettime_ns();

	now += boost_time;
	erd850->deboost_time = now;
}

static void signal_boost(struct erd850_power_module *erd850)
{
        sem_post(&erd850->signal_lock);
}

static void* schedtune_deboost_thread(void* arg) {
	struct erd850_power_module *erd850 = (struct erd850_power_module *)arg;

	while(1) {
		sem_wait(&erd850->signal_lock);
		while(1) {
			long long now, sleeptime = 0;

			pthread_mutex_lock(&erd850->lock);

			now = gettime_ns();
			if (erd850->deboost_time > now) {
				sleeptime = erd850->deboost_time - now;
				pthread_mutex_unlock(&erd850->lock);
				nanosleep_ns(sleeptime);
				continue;
			}

			erd850->deboost_time = 0;
			interactive_power_init(erd850);
			schedtune_sysfs_boost(erd850, SCHEDTUNE_BOOST_NORM);
			schedtune_sysfs_prefer_perf(erd850, false);
			cpu_sysfs_boost(erd850, 0, false);
			//gpu_sysfs_boost(erd850, 0, false);

			pthread_mutex_unlock(&erd850->lock);
			break;
		}
	}
	return NULL;
}

static void schedtune_power_init(struct erd850_power_module *erd850) {
	char buf[50];
	pthread_t tid;

	erd850->deboost_time = 0;
	sem_init(&erd850->signal_lock, 0, 1);

	erd850->schedtune_boost_fd = open(SCHEDTUNE_BOOST_PATH, O_WRONLY);
	if (erd850->schedtune_boost_fd < 0) {
		strerror_r(errno, buf, sizeof(buf));
		ALOGE("Error opening %s: %s\n", SCHEDTUNE_BOOST_PATH, buf);
	}

	erd850->schedtune_prefer_perf_fd = open(SCHEDTUNE_PREFER_PERF_PATH, O_WRONLY);
	if (erd850->schedtune_prefer_perf_fd < 0) {
		strerror_r(errno, buf, sizeof(buf));
		ALOGE("Error opening %s: %s\n", SCHEDTUNE_PREFER_PERF_PATH, buf);
	}

	pthread_create(&tid, NULL, schedtune_deboost_thread, erd850);
}

/*[generic functions]*********************************************************/
static void erd850_power_init(struct power_module __unused *module) {
	struct erd850_power_module *erd850 =
		container_of(module, struct erd850_power_module, base);

	erd850->low_power_mode = false;
	interactive_power_init(erd850);
	schedtune_power_init(erd850);
}


static void erd850_hint_interaction(struct erd850_power_module *mod) {
	expand_boost_time(mod, INTERACTION_BOOST_TIME_NS);
	preper_perf_boost(mod, true);
	schedtune_boost(mod, SCHEDTUNE_BOOST_INTERACTIVE);
	cpu_boost(mod, 936000, true);
	//gpu_boost(mod, 949, true);
	signal_boost(mod);
}

static void erd850_hint_launch(struct erd850_power_module *mod) {
	expand_boost_time(mod, LAUNCH_BOOST_TIME_NS);
	preper_perf_boost(mod, true);
	schedtune_boost(mod, SCHEDTUNE_BOOST_APP_LAUNCH);
	cpu_boost(mod, 2314000, true);
	//gpu_boost(mod, 949, true);
	signal_boost(mod);
}

static void erd850_power_hint(struct power_module *module, power_hint_t hint, void* __unused data) {
	struct erd850_power_module *erd850 = (struct erd850_power_module *) module;

	if (erd850->low_power_mode && hint != POWER_HINT_LOW_POWER)
		return ;

	pthread_mutex_lock(&erd850->lock);

	switch (hint) {
		case POWER_HINT_LOW_POWER:
			if (data) {
				ALOGE("LOW_POWER_MODE is ACTIVATED\n");
			} else {
				ALOGE("LOW_POWER_MODE is DEACTIVATED\n");
			}
			erd850->low_power_mode = data;
			break;
		case POWER_HINT_INTERACTION:
			erd850_hint_interaction(erd850);
			ALOGD("POWER_HINT_INTERACTION\n");
			break;
		case POWER_HINT_LAUNCH:
			erd850_hint_launch(erd850);
			ALOGD("POWER_HINT_LAUNCH\n");
			break;
		case POWER_HINT_VSYNC:
			break;

		default:
			break;
	}

	pthread_mutex_unlock(&erd850->lock);
}

static struct hw_module_methods_t power_module_methods = {
	.open = NULL,
};

struct erd850_power_module HAL_MODULE_INFO_SYM = {
	.base = {
		.common = {
			.tag = HARDWARE_MODULE_TAG,
			.module_api_version = POWER_MODULE_API_VERSION_0_2,
			.hal_api_version = HARDWARE_HAL_API_VERSION,
			.id = POWER_HARDWARE_MODULE_ID,
			.name = "ERD850Power HAL",
			.author = "The Android Open Source Project",
			.methods = &power_module_methods,
		},
		.init = erd850_power_init,
		.powerHint = erd850_power_hint,
	},
	.lock = PTHREAD_MUTEX_INITIALIZER,
};
