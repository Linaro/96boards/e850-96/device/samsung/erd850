#
# Copyright Samsung Electronics Co., LTD.
#
# This software is proprietary of Samsung Electronics.
# No part of this software, either material or conceptual may be copied or distributed, transmitted,
# transcribed, stored in a retrieval system or translated into any human or computer language in any form by any means,
# electronic, mechanical, manual or otherwise, or disclosed
# to third parties without the express written permission of Samsung Electronics.
#
#SSCR Factory Tools Daemon

LOCAL_PATH := $(call my-dir)

#include $(CLEAR_VARS)
#
#LOCAL_MODULE_TAGS := optional
#
#LOCAL_MODULE := libsftd_customer
#
#LOCAL_SHARED_LIBRARIES := \
#    libcutils \
#    liblog
#
#LOCAL_C_INCLUDES += $(LOCAL_PATH)
#
#LOCAL_SRC_FILES += \
#	customerconsole.cpp
#
#include $(BUILD_SHARED_LIBRARY)


#include $(CLEAR_VARS)
#$(info build pcba command)
#LOCAL_SRC_FILES := PCBACommand.cfg
#LOCAL_MODULE := PCBACommand
#LOCAL_MODULE_OWNER := samsung
#LOCAL_MODULE_SUFFIX := .cfg
#LOCAL_MODULE_CLASS := ETC
#LOCAL_MODULE_TAGS := optional
#LOCAL_VENDOR_MODULE := true
#include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
$(info build customer command)
LOCAL_SRC_FILES := CustomCommand.cfg
LOCAL_MODULE := CustomCommand
LOCAL_MODULE_OWNER := samsung
LOCAL_MODULE_SUFFIX := .cfg
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := optional
LOCAL_VENDOR_MODULE := true
include $(BUILD_PREBUILT)

