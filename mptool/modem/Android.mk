LOCAL_PATH:= $(call my-dir)

##################################################
include $(CLEAR_VARS)

#LOCAL_C_INCLUDES := $(LOCAL_PATH)/include

LOCAL_SRC_FILES := modem.cpp

LOCAL_SHARED_LIBRARIES += \
    libhardware \
    libhardware_legacy \
    libcutils \
    libutils \
    libtinyalsa \
    libc \
    libz \
    libbase \
    liblog \
    libz \

LOCAL_SHARED_LIBRARIES += \
    libhidlbase \
    libhidltransport \


LOCAL_LDLIBS := -llog
LOCAL_CFLAGS += -Wall -Wno-unused-parameter
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE:= libsftd_extmodem
include $(BUILD_SHARED_LIBRARY)
