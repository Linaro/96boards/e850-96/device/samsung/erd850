/*
 * Copyright Samsung Electronics Co., LTD.
 *
 * This software is proprietary of Samsung Electronics.
 * No part of this software, either material or conceptual may be copied or distributed, transmitted,
 * transcribed, stored in a retrieval system or translated into any human or computer language in any form by any
means,
 * electronic, mechanical, manual or otherwise, or disclosed
 * to third parties without the express written permission of Samsung Electronics.
 */

/*
    MODEM COMMAND class
*/
#ifndef __MODEM_COMMAND_H_
#define __MODEM_COMMAND_H_

using namespace std;

#define AT_OK_RESPONSE      "\r\nOK\r\n"
#define AT_ERROR_RESPONSE      "\r\nERROR\r\n"
#define AT_CMD_ENG_SYMBOL   "\r\n"
#define COLON_SYMBOL        ":"
#define COMMA_SYMBOL        ","


#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

int SendCustomerCommand(int event, uint8_t* cmd, int len, uint8_t **out, int *olen);

int ExecModemCommand(char* cmd, int len, char **out, int *olen, int (*func)(const uint8_t*, int, uint8_t**, int*));
bool CheckModemCommand(const char* cmd, int len);
const char** ExportModemCommand();
void GetCmdValueHead(const char* cmd, char* strHead);

#ifdef __cplusplus
}
#endif
#endif

