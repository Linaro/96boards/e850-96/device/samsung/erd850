/*
 * Copyright Samsung Electronics Co., LTD.
 *
 * This software is proprietary of Samsung Electronics.
 * No part of this software, either material or conceptual may be copied or distributed, transmitted,
 * transcribed, stored in a retrieval system or translated into any human or computer language in any form by any
means,
 * electronic, mechanical, manual or otherwise, or disclosed
 * to third parties without the express written permission of Samsung Electronics.
 */

/*
    Response service class implementation
*/
#include "modem.h"
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <cutils/sockets.h>



/**********************************************SFTD START**********************************************/

#define LOG_TAG    "SFTD"
#include "log/log.h"

#define SFTD_COMMAND_READ_IMEI       "SAT+RDIMEI"
#define SFTD_COMMAND_WRITE_IMEI      "SAT+WRIMEI"
#define SFTD_COMMAND_READ_MEID       "SAT+RDMEID"
#define SFTD_COMMAND_WRITE_MEID      "SAT+WRMEID"
#define SFTD_COMMAND_AIRPLANE        "SAT+AIRPLANE"
#define SFTD_COMMAND_CHECK_SIM       "SAT+SIM"
#define SFTD_COMMAND_SLEEP           "SAT+CPSLEEP"
#define SFTD_COMMAND_VERSION         "SAT+CPVERSION"
#define SFTD_COMMAND_CHECKRF         "SAT+RFDETECT"
#define SFTD_COMMAND_SIMTRAY         "SAT+SIMTRAY"
#define SFTD_COMMAND_SIMCOUNT        "SAT+SIMCOUNT"
#define SFTD_COMMAND_SIMINFO         "SAT+SIMINFO"




static const char* gpSftdCmd[] = {
    SFTD_COMMAND_READ_IMEI,
    SFTD_COMMAND_WRITE_IMEI,
    SFTD_COMMAND_READ_MEID,
    SFTD_COMMAND_WRITE_MEID,
    SFTD_COMMAND_AIRPLANE,
    SFTD_COMMAND_CHECK_SIM,
    SFTD_COMMAND_SLEEP,
    SFTD_COMMAND_VERSION,
    SFTD_COMMAND_CHECKRF,
    SFTD_COMMAND_SIMTRAY,
    SFTD_COMMAND_SIMCOUNT,
    SFTD_COMMAND_SIMINFO,
    nullptr,/*last command*/
};

enum CmdType{
    READ_SIM1       =       0,
    WRITE_SIM1      =       1,
    READ_SIM2       =       2,
    WRITE_SIM2      =       3,
    CLOSE_AIRPLANE  =       4,
    OPEN_AIRPLANE   =       5,
    CHECK_SIM1      =       6,
    CHECK_SIM2      =       7,
    CP_SLEEP        =       8,
    CP_VERSION      =       9,
    RF_DETECT       =       10,
    SIM_TRAY        =       11,
    SIM_COUNT       =       12,
    SIM_INFO        =       13,
    READ_MEID       =       14,
    WRITE_MEID      =       15,
    CMD_MAX         =       16
};



/*for real sat command*/
struct{
    const char* satcmd;
    const char* atcmd;
    int type;
} gpExtModemCmd[] = {
    {"SAT+RDIMEI=1",                          "AT+SNINFO=0,1\r\n",         READ_SIM1},
    {"SAT+WRIMEI=1,",                         "AT+SNWRITE=0,1,",           WRITE_SIM1},/*imcomplete command*/
    {"SAT+RDIMEI=2",                          "AT+SNINFO=1,1\r\n",         READ_SIM2},
    {"SAT+WRIMEI=2,",                         "AT+SNWRITE=1,1,",           WRITE_SIM2},/*imcomplete command*/
    {"SAT+RDMEID",                            "AT+SNINFO=0,2\r\n",         READ_MEID},
    {"SAT+WRMEID=",                           "AT+SNWRITE=0,2,",           WRITE_MEID},/*imcomplete command*/
    {"SAT+AIRPLANE=0",                        "AT+CFUN=0\r\n",             CLOSE_AIRPLANE},
    {"SAT+AIRPLANE=1",                        "AT+CFUN=1\r\n",             OPEN_AIRPLANE},
    {"SAT+SIM=1",                             "AT+SIMPRESENT=0,1\r\n",     CHECK_SIM1},
    {"SAT+SIM=2",                             "AT+SIMPRESENT=0,2\r\n",     CHECK_SIM2},
    {SFTD_COMMAND_SLEEP,                      "AT+CPSLEEP=0,0\r\n",        CP_SLEEP},
    {SFTD_COMMAND_VERSION,                    "AT+SWVERSION?\r\n",         CP_VERSION},
    {SFTD_COMMAND_CHECKRF,                    "AT+RFCALCONDUCT\r\n",       RF_DETECT},
    {SFTD_COMMAND_SIMTRAY,                    "AT+SIMTRAYSTATUS?\r\n",     SIM_TRAY},
    {SFTD_COMMAND_SIMCOUNT,                   "AT+SIMPRESENT?\r\n",        SIM_COUNT},
    {SFTD_COMMAND_SIMINFO,                    "AT+READSIMINFO",            SIM_INFO},
    {nullptr,                                 nullptr,                     CMD_MAX},
};

static int gExtModemSize = sizeof(gpExtModemCmd)/ sizeof(gpExtModemCmd[0]);

int GetIndexBySATCmd(const char* cmd)
{
    const char* satcmd = nullptr;
    for (int i = 0; i < gExtModemSize - 1; i++)
    {
        satcmd = gpExtModemCmd[i].satcmd;
        if(satcmd == nullptr)
        {
            ALOGD("%s::sat ext-modem command is nullptr", __FUNCTION__);
            continue;
        }
        //ALOGD("%s::internal cmd(%s), original cmd(%s)", __FUNCTION__, satcmd, cmd);

        if(strncmp(satcmd, cmd, strlen(satcmd)) == 0)
        {
            ALOGD("%s:: get ext-modem command type(%s)", __FUNCTION__, satcmd);
            return i;
        }
    }
    return CMD_MAX;
}
//(const uint8_t* cmd, int length, uint8_t** resp, int* resplen)

int ExecModemCommand(char* cmd, int len, char **out, int *olen, int (*func)(const uint8_t*, int, uint8_t**, int*))
{
    string respStr = cmd;
    string str;

    size_t endPos = string::npos;
    size_t startPos = string::npos;
    uint8_t* resp = nullptr;
    int resplen = 0;
    string atCmd;
    string retStr;

    int ret = 1;
    int index = CMD_MAX;

    if(func == nullptr)
    {
        ALOGE("%s:: NO modem function", __FUNCTION__);
        goto BAD;
    }

    index = GetIndexBySATCmd(cmd);
    if(gpExtModemCmd[index].atcmd == nullptr)
    {
        ALOGE("%s:: NO support at command", __FUNCTION__);
        goto BAD;
    }

    atCmd = gpExtModemCmd[index].atcmd;

    switch(gpExtModemCmd[index].type)
    {
        case CP_SLEEP:
            //sleep command
            //return func((const uint8_t *)atCmd.c_str(), atCmd.length(), nullptr, nullptr, nullptr);
            ret = func((const uint8_t *)atCmd.c_str(), atCmd.length(), &resp, &resplen);
            break;
        case CMD_MAX:
            break;
        case WRITE_SIM1:/*write imei1 command*/
        case WRITE_SIM2:/*write imei2 command*/
        case WRITE_MEID:/*write imei2 command*/
        case SIM_INFO:
        {
            const char* satcmd = gpExtModemCmd[index].satcmd;
            char* value = cmd + strlen(satcmd);
            atCmd += value;
            atCmd += AT_CMD_ENG_SYMBOL;
            ret = func((const uint8_t *)atCmd.c_str(), atCmd.length(), &resp, &resplen);
        }
            break;
        default:
            ret = func((const uint8_t *)atCmd.c_str(), atCmd.length(), &resp, &resplen);
            break;
    }

    if((ret != 0) || (resp == nullptr) || (resplen == 0))
    {
        ALOGE("%s:: at command response error", __FUNCTION__);
        goto BAD;
    }

    ALOGD("%s:: at command response: %s", __FUNCTION__, resp);

    str = (const char *)resp;

    if((READ_SIM1 == gpExtModemCmd[index].type) || (READ_SIM2 == gpExtModemCmd[index].type))
    {
        //read test imei1
        if((endPos = str.find(AT_OK_RESPONSE)) != string::npos)
        {
            if((startPos = str.find(COLON_SYMBOL)) != string::npos)
            {
                ++startPos; //ignore colon symbol":"
                startPos = startPos + 2; //ignore sim slot No.    //jim
                int len = endPos - startPos - 2;
                //OK response string format:AT+command\r\n+command:[result string]\r\n\r\nOK\r\n
                //FAIL response string format:AT+command\r\n\r\nERROR\r\n
                string cutstr = str.substr(startPos, len);
                ALOGD("[%s]::[%s]", __FUNCTION__, cutstr.c_str());

                cutstr += AT_CMD_ENG_SYMBOL;  //jim
                cutstr += AT_OK_RESPONSE;
                string headstr = "\r\n+RDIMEI:" + cutstr;
                /*cmd   \r\n                        +RDIMEI:         imei info    \r\n\r\nOK\r\n*/
                *olen = strlen(cmd) + headstr.length();    //jim
                *out =  new char[*olen + 1];
                memset(*out, 0, *olen + 1);
                memcpy(*out, cmd, strlen(cmd));   //jim
                memcpy(*out + strlen(cmd), headstr.c_str(), headstr.length());  //jim
                ALOGD("[%s]:out:[%s], *olen = %d", __FUNCTION__, *out, *olen);
                return 0;
            }
        }
    }
    else if((CHECK_SIM1 == gpExtModemCmd[index].type) || (CHECK_SIM2 == gpExtModemCmd[index].type))  //add by jim
    {
        //sim presen test
        if((startPos = str.find("SIMPRESENT:OK")) != string::npos)
        {
            string cutstr = AT_CMD_ENG_SYMBOL;
            cutstr += AT_OK_RESPONSE;
            *olen = strlen(cmd) + cutstr.length();
            *out =  new char[*olen + 1];
            memset(*out, 0, *olen + 1);
            memcpy(*out, cmd, strlen(cmd));
            memcpy(*out + strlen(cmd), cutstr.c_str(), cutstr.length());
            ALOGD("[%s]:out:[%s], *olen = %d", __FUNCTION__, *out, *olen);
            return 0;
        }
    }
    else if(RF_DETECT == gpExtModemCmd[index].type)
    {
        ALOGD("[%s]:RFDETECT response parser", __FUNCTION__);
        if((endPos = str.rfind(AT_OK_RESPONSE)) != string::npos)
        {
            if((startPos = str.find(COLON_SYMBOL)) != string::npos)
            {
                ALOGD("[%s]:RFDETECT response include return value", __FUNCTION__);
                ++startPos; //ignore colon symbol":"
                if((endPos = str.rfind("\r\n\r\nOK\r\n")) != string::npos){
                    int len = endPos - startPos;
                    string cutstr = str.substr(startPos, len);
                    ALOGD("[%s]:RFDETECT value = %s", __FUNCTION__, cutstr.c_str());
                    if(atoi(cutstr.c_str()) == 1){
                        goto GOOD;
                    }
                }
            }
        }
    }
    else if(CP_VERSION == gpExtModemCmd[index].type)
    {
        //read cp version
        if((endPos = str.find(AT_OK_RESPONSE)) != string::npos)
        {
            if((startPos = str.find(COLON_SYMBOL)) != string::npos)
            {
                ++startPos; //ignore colon symbol":"
                int lenv = endPos - startPos - 2;
                //OK response string format:AT+command\r\n+command:[result string]\r\n\r\nOK\r\n
                //FAIL response string format:AT+command\r\n\r\nERROR\r\n
                string cutstr = str.substr(startPos, lenv);
                ALOGD("[%s]::[%s]", __FUNCTION__, cutstr.c_str());

                cutstr += AT_CMD_ENG_SYMBOL;  //jim
                cutstr += AT_OK_RESPONSE;
                string headstr = "\r\n+CPVERSION:" + cutstr;
                /*cmd   \r\n                        +CPVERSION:         version info    \r\n\r\nOK\r\n*/
                *olen = len + headstr.length();
                *out =  new char[*olen + 1];
                memset(*out, 0, *olen + 1);
                memcpy(*out, cmd, len);
                memcpy(*out + len, headstr.c_str(), headstr.length());
                ALOGD("[%s]:out:[%s], *olen = %d", __FUNCTION__, *out, *olen);
                return 0;
            }
        }
    } 
    else if((SIM_TRAY == gpExtModemCmd[index].type))
    {
        //simtray present test
        if((endPos = str.rfind(AT_OK_RESPONSE)) != string::npos)
        {
            if((startPos = str.find(COLON_SYMBOL)) != string::npos)
            {
                ++startPos; //ignore colon symbol":"
                int len = endPos - startPos - 2;
                string cutstr = str.substr(startPos, len);
                //ALOGD("[%s]::startPos:%d, endPos:%d, len:%d, cutstr:%s", __FUNCTION__, startPos, endPos, len,cutstr.c_str());
                ALOGD("[%s]::[%s]", __FUNCTION__, cutstr.c_str());

                cutstr += AT_CMD_ENG_SYMBOL;
                cutstr += AT_OK_RESPONSE;
                string headstr = cmd;
                char strTemp[64] = {0,};
                GetCmdValueHead(cmd, strTemp);
                headstr += strTemp + cutstr;

                *olen = headstr.length();
                *out =  new char[*olen + 1];
                memset(*out, 0, *olen + 1);
                memcpy(*out, headstr.c_str(), headstr.length());
                return 0;
            }
        }
    }
    else if((SIM_INFO == gpExtModemCmd[index].type) || (READ_MEID == gpExtModemCmd[index].type))
    {
        //sim count test
        if((endPos = str.rfind(AT_OK_RESPONSE)) != string::npos)
        {
            if((startPos = str.find(COLON_SYMBOL)) != string::npos)
            {
                //AT+READSIMINFO=1,1<CR><LF><CR><LF>+READSIMINFO:1,460006811110830<CR><LF><CR><LF>OK<CR><LF>
                startPos += 3;//ignore ":1,",just get value
                int len = endPos - startPos - 2;
                string cutstr = str.substr(startPos, len);
                //ALOGD("[%s]::startPos:%d, endPos:%d, len:%d, cutstr:%s", __FUNCTION__, startPos, endPos, len,cutstr.c_str());
                ALOGD("[%s]::[%s]", __FUNCTION__, cutstr.c_str());

                cutstr += AT_CMD_ENG_SYMBOL;
                cutstr += AT_OK_RESPONSE;
                string headstr = cmd;
                char strTemp[64] = {0,};
                GetCmdValueHead(cmd, strTemp);
                headstr += strTemp + cutstr;

                *olen = headstr.length();
                *out =  new char[*olen + 1];
                memset(*out, 0, *olen + 1);
                memcpy(*out, headstr.c_str(), headstr.length());
                return 0;
            }
        }
    }
    else if((SIM_COUNT == gpExtModemCmd[index].type))
    {
        //sim count test
        if((endPos = str.rfind(AT_OK_RESPONSE)) != string::npos)
        {
            if((startPos = str.find(COLON_SYMBOL)) != string::npos)
            {
                ++startPos; //ignore colon symbol":"
                int len = endPos - startPos - 2;
                string cutstr = str.substr(startPos, len);
                //ALOGD("[%s]::startPos:%d, endPos:%d, len:%d, cutstr:%s", __FUNCTION__, startPos, endPos, len,cutstr.c_str());
                ALOGD("[%s]::[%s]", __FUNCTION__, cutstr.c_str());
                int iSimCnt = atoi(cutstr.c_str());
                if((iSimCnt == 1) || (iSimCnt == 2)){
                    cutstr = "1";
                }else if(iSimCnt == 3){
                    cutstr = "2";
                }else{
                    cutstr = "0";
                }

                cutstr += AT_CMD_ENG_SYMBOL;
                cutstr += AT_OK_RESPONSE;
                string headstr = cmd;
                char strTemp[64] = {0,};
                GetCmdValueHead(cmd, strTemp);
                headstr += strTemp + cutstr;

                *olen = headstr.length();
                *out =  new char[*olen + 1];
                memset(*out, 0, *olen + 1);
                memcpy(*out, headstr.c_str(), headstr.length());
                return 0;
            }
        }
    }
    else if((endPos = str.find(AT_OK_RESPONSE)) != string::npos)
    {
        goto GOOD;
    }

BAD:
    retStr = AT_CMD_ENG_SYMBOL;
    retStr += AT_ERROR_RESPONSE;
    *olen = len + retStr.length();
    *out = new char[*olen + 1];
    memset(*out, 0, (*olen + 1));
    memcpy(*out, cmd, len);
    memcpy(*out + len, retStr.c_str(), retStr.length());
    ALOGD("[%s]:out:[%s], *olen = %d", __FUNCTION__, *out, *olen);
    return ret;

GOOD:
    retStr = AT_CMD_ENG_SYMBOL;
    retStr += AT_OK_RESPONSE;
    *olen = len + retStr.length();
    *out = new char[*olen + 1];
    memset(*out, 0, (*olen + 1));
    memcpy(*out, cmd, len);
    memcpy(*out + len, retStr.c_str(), retStr.length());
    ALOGD("[%s]:out:[%s], *olen = %d", __FUNCTION__, *out, *olen);
    return ret;

LOCKWRITE_BAD:
    retStr = "SAT+LOCKWRITE\r\n\r\nERROR\r\n";
    *olen = retStr.length();
    *out = new char[*olen + 1];
    memset(*out, 0, (*olen + 1));
    memcpy(*out, retStr.c_str(), retStr.length());
    ALOGD("[%s]:out:[%s], *olen = %d", __FUNCTION__, *out, *olen);
    return ret;
}

bool CheckModemCommand(const char* cmd, int len)
{
    ALOGD("CheckModemCommand:: command is %s", cmd);
    int cmdcount = (sizeof(gpSftdCmd) / sizeof(gpSftdCmd[0]) - 1);
    for (int i = 0; i < cmdcount; i++)
    {
        if(strncmp(gpSftdCmd[i], cmd, strlen(gpSftdCmd[i])) == 0)
        {
            ALOGD("CheckModemCommand:: find ext-modem command(%s)", cmd);
            return true;
        }
    }
    return false;
}

const char**  ExportModemCommand()
{
    return gpSftdCmd;
}

void GetCmdValueHead(const char* cmd, char* strHead){
    if((cmd == NULL) || (strlen(cmd) == 0) || (strHead == NULL)){
        return;
    }

    ALOGD("%s:: cmd:%s", __FUNCTION__, cmd);
    string valueCmdHead = AT_CMD_ENG_SYMBOL;
    string strCmd = cmd;
    int len = 0;
    int pos1 = string::npos;
    int pos2 = string::npos;
    if((pos1 = strCmd.find('+')) != string::npos){
        if((pos2 = strCmd.find('=')) != string::npos){
            len = pos2 - pos1;
        }else{
            len = strlen(cmd) - pos1;
        }
        valueCmdHead += strCmd.substr(pos1,len);
        valueCmdHead += COLON_SYMBOL;
        strcpy(strHead, valueCmdHead.c_str());
        ALOGD("%s:: strHead:%s", __FUNCTION__, strHead);
    }

    return;
}

/**********************************************SFTD END**********************************************/

