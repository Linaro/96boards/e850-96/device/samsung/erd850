/*************************************************************************
	> File Name: vendor/samsung_slsi/engmode/vendor/backlight/backlight_test.h
	> Author:Yan Minggui
	> Mail: minggui.yan@samsung.com
	> Created Time: Thu 14 Jun 2018 09:39:30 AM CST
 ************************************************************************/
#ifndef __BACKLIGHT_TEST_H__
#define __BACKLIGHT_TEST_H__

int setBacklight(int color);
int openBacklight();

#endif
