# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for smdk850 hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and smdk850, hence its name.
#

ifeq ($(TARGET_PRODUCT),full_smdk850)

# add launcer
PRODUCT_PACKAGES += \
    Launcher3

# Live Wallpapers
PRODUCT_PACKAGES += \
        LiveWallpapers \
        LiveWallpapersPicker \
        MagicSmokeWallpapers \
        VisualizationWallpapers \
        librs_jni

PRODUCT_PROPERTY_OVERRIDES := \
        net.dns1=8.8.8.8 \
        net.dns2=8.8.4.4

# Inherit from those products. Most specific first.
$(call inherit-product, device/samsung/erd850/device.mk)

#
# ADDITIONAL VENDOR BUILD PROPERTIES (Telephony)
#
PRODUCT_PROPERTY_OVERRIDES += \
    persist.radio.multisim.config=dsds

PRODUCT_PROPERTY_OVERRIDES += \
    ro.telephony.default_network=10 \
    ro.logd.size=16M \
    telephony.lteOnCdmaDevice=1

TARGET_SOC := exynos850
TARGET_SOC_BASE := exynos850
TARGET_BOARD_PLATFORM := smdk850
TARGET_BOOTLOADER_BOARD_NAME := exynos850

PRODUCT_NAME := full_smdk850
PRODUCT_DEVICE := erd850
PRODUCT_BRAND := Android
PRODUCT_MODEL := Full Android on SMDK850
PRODUCT_MANUFACTURER := Samsung Electronics Co., Ltd.
TARGET_LINUX_KERNEL_VERSION := 4.14

PRODUCT_PACKAGES += ShannonIms

TARGET_BUILD_KERNEL_FROM_SOURCE := true
endif
