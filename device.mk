#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_SHIPPING_API_LEVEL := 29

include $(LOCAL_PATH)/BoardConfig.mk

BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts

INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
PRODUCT_COPY_FILES += $(foreach image,\
		      $(wildcard $(BOARD_PREBUILTS)/*),\
		      $(image):$(PRODUCT_OUT)/$(notdir $(image)))

# for tunnel
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.ipsec_tunnels.xml:vendor/etc/permissions/android.software.ipsec_tunnels.xml

# Enable Dynamic Partitions
PRODUCT_USE_DYNAMIC_PARTITIONS := true
BOARD_BUILD_SYSTEM_ROOT_IMAGE := false

# System AS Root & Dynamic Partition support handle
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS), true)
# Dynamic Partitions options & config
BOARD_SUPER_PARTITION_SIZE := 3774873600
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 3770679296
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
BOARD_BUILD_SUPER_IMAGE_BY_DEFAULT := true
BOARD_SUPER_IMAGE_IN_UPDATE_PACKAGE := true
endif

# For A/B device
ifeq ($(AB_OTA_UPDATER),true)
AB_OTA_PARTITIONS := \
  vbmeta \
  boot \
  system \
  dtbo \
  vendor
BOARD_USES_RECOVERY_AS_BOOT := true
PRODUCT_PACKAGES += \
  android.hardware.boot@1.0-impl \
  android.hardware.boot@1.0-service \
  bootctrl.$(TARGET_BOOTLOADER_BOARD_NAME) \
  bootctl \
  update_engine \
  update_verifier
endif

# recovery mode
BOARD_INCLUDE_RECOVERY_DTBO := true
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_BOOTIMG_HEADER_VERSION := 2
ifeq ($(AB_OTA_UPDATER),true)
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
  --recovery_dtbo $(BOARD_PREBUILT_DTBOIMAGE) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset 0
else
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset 0
endif

# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    dev.usbsetting.embedded=on \
    ro.vendor.config.release_version=20190306

# Device Manifest, Device Compatibility Matrix for Treble
DEVICE_MANIFEST_FILE := \
    device/samsung/erd850/manifest.xml

DEVICE_MATRIX_FILE := \
    device/samsung/erd850/compatibility_matrix.xml

# These are for the multi-storage mount.
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erd850/overlay-sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erd850/overlay-ufsboot
else
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/erd850/overlay-emmcboot
endif
endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/erd850/overlay

# Init files
PRODUCT_COPY_FILES += \
	device/samsung/erd850/conf/init.exynos850.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.exynos850.rc \
	device/samsung/erd850/conf/init.exynos850.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.exynos850.usb.rc \
	device/samsung/erd850/conf/ueventd.exynos850.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \
	device/samsung/erd850/conf/init.recovery.exynos850.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.recovery.exynos850.rc \
	device/samsung/erd850/conf/init.recovery.exynos850.rc:root/init.recovery.exynos850.rc

# for off charging mode
PRODUCT_PACKAGES += \
	charger_res_images

ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	device/samsung/erd850/conf/fstab.exynos850.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.exynos850
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
ifeq ($(BOARD_USE_FACTORY_IMAGES),true)
PRODUCT_COPY_FILES += \
        device/samsung/erd850/mptool/conf/fstab.exynos850:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.exynos850
else
PRODUCT_COPY_FILES += \
	device/samsung/erd850/conf/fstab.exynos850:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.exynos850
endif #BOARD_USE_FACTORY_IMAGES
else
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
PRODUCT_COPY_FILES += \
	device/samsung/erd850/conf/fstab.exynos850.emmc.dp:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.exynos850 \
	device/samsung/erd850/conf/fstab.exynos850.emmc.dp:$(TARGET_COPY_OUT_RAMDISK)/fstab.exynos850 \
	device/samsung/erd850/conf/fstab.exynos850.emmc.dp:root/fstab.exynos850
TARGET_RECOVERY_FSTAB := device/samsung/erd850/conf/fstab.exynos850.emmc.dp
else
PRODUCT_COPY_FILES += \
	device/samsung/erd850/conf/fstab.exynos850.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.exynos850 \
	device/samsung/erd850/conf/fstab.exynos850.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.exynos850 \
	device/samsung/erd850/conf/fstab.exynos850.emmc:root/fstab.exynos850
TARGET_RECOVERY_FSTAB := device/samsung/erd850/conf/fstab.exynos850.emmc
endif #BOARD_USE_DYNAMIC_PARTITIONS
endif #BOARD_USE_FACTORY_IMAGES
endif #BOARD_USES_SDMMC_BOOT

# Support devtools
PRODUCT_PACKAGES += \
	Development

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA
PRODUCT_PACKAGES += \
	tlrpmb

# RPMB test application (only for eng build)
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	bRPMB
endif

# CBD (CP booting deamon)
#CBD_USE_V2 := true
#CBD_PROTOCOL_SIT := true

#BOARD_USE_FM_RADIO := true
# FM Test App
#PRODUCT_PACKAGES += \
#	FMRadioService \
#	privapp-permissions-fmradio.xml

# boot animation files
PRODUCT_COPY_FILES += \
    device/samsung/erd850/bootanim/bootanimation_part1.zip:system/media/bootanimation.zip \
    device/samsung/erd850/bootanim/shutdownanimation.zip:system/media/shutdownanimation.zip

# color mode
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.sf.color_saturation=1.0

# dqe calib xml
#PRODUCT_COPY_FILES += \
    device/samsung/erd850/dqe/hixmax-hix83112a.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/hixmax-hix83112a.xml \
    device/samsung/erd850/dqe/novatek-nov36672a.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/novatek-nov36672a.xml \
    device/samsung/erd850/dqe/samsung-s6e3fa0-vdo.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/samsung-s6e3fa0-vdo.xml \
    device/samsung/erd850/dqe/default-lcd-vdo.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/default-lcd-vdo.xml \
    device/samsung/erd850/dqe/calib_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data.xml

ifeq ($(BOARD_USES_EXYNOS_SENSORS_HAL),true)
# Enable support for chinook sensorhu
TARGET_USES_CHINOOK_SENSORHUB := false

PRODUCT_COPY_FILES += \
    device/samsung/erd850/init.exynos.nanohub.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.exynos.sensorhub.rc \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.gyroscope.xml\
    frameworks/native/data/etc/android.hardware.sensor.light.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.light.xml\
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.compass.xml \
    #frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \
    #device/samsung/erd850/firmware/chub_bl.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin \
    device/samsung/erd850/firmware/chub_nanohub_0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_0.bin \
    device/samsung/erd850/firmware/chub_nanohub_1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_1.bin \

# Copy CHUB os binary file
ifneq ("$(wildcard device/samsung/erd850/firmware/os.checked*.bin)", "")
PRODUCT_COPY_FILES += $(foreach image,\
    $(wildcard device/samsung/erd850/firmware/os.checked*.bin),\
    $(image):$(TARGET_COPY_OUT_VENDOR)/firmware/$(notdir $(image)))
else
PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/chub_nanohub_0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_0.bin \
    device/samsung/erd850/firmware/chub_nanohub_1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_1.bin
endif

# Copy CHUB bl binary file
ifneq ("$(wildcard device/samsung/erd850/firmware/bl.unchecked.bin)", "")
PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/bl.unchecked.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin
else
PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/chub_bl.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin
endif

# Copy CHUB os elf file
ifneq ("$(wildcard device/samsung/erd850/firmware/os.checked*.elf)", "")
PRODUCT_COPY_FILES += $(foreach image,\
    $(wildcard device/samsung/erd850/firmware/os.checked*.elf),\
    $(image):$(TARGET_COPY_OUT_VENDOR)/firmware/$(notdir $(image)))
endif
# Copy CHUB bl elf file
ifneq ("$(wildcard device/samsung/erd850/firmware/bl.unchecked.elf)", "")
PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/bl.unchecked.elf:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.elf
endif

# Sensor HAL
TARGET_USES_NANOHUB_SENSORHAL := true
NANOHUB_SENSORHAL_SENSORLIST := $(LOCAL_PATH)/sensorhal/sensorlist.cpp
NANOHUB_SENSORHAL_EXYNOS_CONTEXTHUB := true
NANOHUB_SENSORHAL_NAME_OVERRIDE := sensors.$(TARGET_BOARD_PLATFORM)

PRODUCT_PACKAGES += \
    $(NANOHUB_SENSORHAL_NAME_OVERRIDE) \
    activity_recognition.erd850 \
    context_hub.default \
    android.hardware.sensors@1.0-impl \
    android.hardware.sensors@1.0-service

# sensor utilities
PRODUCT_PACKAGES += \
    nanoapp_cmd

# sensor utilities (only for userdebug and eng builds)
ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
    nanotool \
    sensortest
endif
endif

ifeq ($(BOARD_USES_EXYNOS_SENSORS_DUMMY), true)
# Sensor HAL
PRODUCT_PACKAGES += \
	android.hardware.sensors@1.0-impl \
	android.hardware.sensors@1.0-service \
	sensors.$(TARGET_SOC)
endif

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service.$(TARGET_SOC)

# Lights HAL
PRODUCT_PACKAGES += \
	lights.$(TARGET_SOC) \
	android.hardware.light@2.0-impl \
	android.hardware.light@2.0-service

# Fastboot HAL
PRODUCT_PACKAGES += \
       fastbootd \
       android.hardware.fastboot@1.0\
       android.hardware.fastboot@1.0-impl-mock.exynos850\

# Power HAL
PRODUCT_PACKAGES += \
	android.hardware.power@1.0-impl \
	android.hardware.power@1.0-service \
	power.$(TARGET_SOC)

# configStore HAL
PRODUCT_PACKAGES += \
	android.hardware.configstore@1.0-service \
	android.hardware.configstore@1.0-impl \
	vendor.samsung_slsi.hardware.configstore@1.0-service \
	vendor.samsung_slsi.hardware.configstore@1.0-impl

# OFI HAL
# PRODUCT_PACKAGES += \
  vendor.samsung_slsi.hardware.ofi@1.0 \
  vendor.samsung_slsi.hardware.ofi@1.0-service
#  vendor.samsung_slsi.hardware.ofi@1.0-service-lazy

#PRODUCT_COPY_FILES += \
  device/samsung/erd850/firmware/CC_DRAM_CODE_FLASH.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DRAM_CODE_FLASH.bin \
  device/samsung/erd850/firmware/CC_DTCM_CODE_FLASH.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DTCM_CODE_FLASH.bin \
  device/samsung/erd850/firmware/CC_ITCM_CODE_FLASH.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_ITCM_CODE_FLASH.bin \
  device/samsung/erd850/firmware/kernel_bin_enf.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/kernel_bin_enf.bin \
  device/samsung/erd850/firmware/kernel_bin_inception.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/kernel_bin_inception.bin \
  device/samsung/erd850/firmware/kernel_bin_mobile_vgg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/kernel_bin_mobile_vgg.bin
# Thermal HAL
PRODUCT_PACKAGES += \
	android.hardware.thermal@1.0-impl\
	android.hardware.thermal@1.0-service\
	thermal.$(TARGET_SOC)

#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.0-service

# Audio HALs
# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := true
USE_XML_AUDIO_POLICY_CONF := 1

# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@5.0-impl \
	android.hardware.audio.effect@5.0-impl \
	android.hardware.soundtrigger@2.0-impl

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.a2dp.default \
	audio.r_submix.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	frameworks/av/services/audiopolicy/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/a2dp_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
	device/samsung/erd850/audio/config/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	device/samsung/erd850/audio/config/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/erd850/audio/config/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml \
	device/samsung/erd850/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

# Calliope firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/erd850/firmware/audio/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/erd850/firmware/audio/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/erd850/firmware/audio/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/erd850/firmware/audio/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/erd850/firmware/audio/abox_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.conf \
	device/samsung/erd850/firmware/audio/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/erd850/firmware/audio/param_aprxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aprxse.bin \
	device/samsung/erd850/firmware/audio/param_aptxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aptxse.bin \
	device/samsung/erd850/firmware/audio/param_cprxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cprxse.bin \
	device/samsung/erd850/firmware/audio/param_cptxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cptxse.bin \
	device/samsung/erd850/firmware/audio/rxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/erd850/firmware/audio/txse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse.bin \
	device/samsung/erd850/firmware/audio/EXYNOS850_ERD_NACHO_slog.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/EXYNOS850_ERD_NACHO_slog.bin

# MIDI support native XML
PRODUCT_COPY_FILES += \
        frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

# Enable AAudio MMAP/NOIRQ data path.
#PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=1
#PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=1
#PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# A-Box Service Daemon
PRODUCT_PACKAGES += main_abox

# control_privapp_permissions
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

# TinyTools for Audio
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo \
    cplay
endif

ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += dssd
endif

# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

# for now include gralloc here. should come from hardware/samsung_slsi/exynos5
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@2.0-impl-2.1 \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)

PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    libion

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
PRODUCT_PACKAGES += \
	VideoEditorGoogle

# WideVine modules
PRODUCT_PACKAGES += \
	android.hardware.drm@1.0-impl \
	android.hardware.drm@1.0-service \
	android.hardware.drm@1.2-service.widevine \
	android.hardware.drm@1.2-service.clearkey \
	move_widevine_data.sh

# SecureDRM modules
PRODUCT_PACKAGES += \
	tlwvdrm \
	tlsecdrm \
	liboemcrypto_modular

# MobiCore namespace
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/kinibi500

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	vendor.trustonic.tee@1.0-service \
	vendor.trustonic.teeregistry@1.0-service \
        vendor.trustonic.tee@2.0-service \
	RootPA \
	TeeService \
	libTeeClient \
	libteeservice_client.trustonic \
	tee_tlcm \
	tee_whitelist

# SPI driver
#PRODUCT_PACKAGES += \
	 tlspidrv

# Camera HAL
PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/camera/is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_lib.bin \
    device/samsung/erd850/firmware/camera/is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/is_rta.bin \
    device/samsung/erd850/firmware/camera/setfile_4ec.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_4ec.bin

# Copy OIS Libraries
#PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/camera/bu24218_cal_data_default.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_cal_data_default.bin \
    device/samsung/erd850/firmware/camera/bu24218_Rev1.5_S_data1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_Rev1.5_S_data1.bin \
    device/samsung/erd850/firmware/camera/bu24218_Rev1.5_S_data2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_Rev1.5_S_data2.bin

# Copy VIPx Firmware
#PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/camera/vipx/CC_DRAM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DRAM_CODE_FLASH_HIFI.bin \
    device/samsung/erd850/firmware/camera/vipx/CC_DTCM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DTCM_CODE_FLASH_HIFI.bin \
    device/samsung/erd850/firmware/camera/vipx/CC_ITCM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_ITCM_CODE_FLASH_HIFI.bin

# Copy Camera HFD Setfiles
#PRODUCT_COPY_FILES += \
    device/samsung/erd850/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/erd850/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/erd850/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/erd850/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	frameworks/native/data/etc/android.hardware.camera.full.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.full.xml \
	frameworks/native/data/etc/android.hardware.camera.raw.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.raw.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml \
	frameworks/native/data/etc/android.hardware.audio.pro.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.pro.xml \

# WLAN configuration
# device specific wpa_supplicant.conf
PRODUCT_COPY_FILES += \
		       device/samsung/erd850/wifi/p2p_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/p2p_supplicant.conf \
		       device/samsung/erd850/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf

# Bluetooth configuration
PRODUCT_COPY_FILES += \
       frameworks/native/data/etc/android.hardware.bluetooth.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth.xml \
       hardware/samsung_slsi/libbt/conf/bt_did.conf:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth/bt_did.conf \
       frameworks/native/data/etc/android.hardware.bluetooth_le.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth_le.xml

PRODUCT_PROPERTY_OVERRIDES += \
        wifi.interface=wlan0

# Packages needed for WLAN
# Note android.hardware.wifi@1.0-service is used by HAL interface 1.2
PRODUCT_PACKAGES += \
    android.hardware.wifi@1.0-service \
    android.hardware.wifi.supplicant@1.1-service \
    android.hardware.wifi.offload@1.0-service \
    dhcpcd.conf \
    hostapd \
    wificond \
    wifilog \
    wpa_supplicant \
    wpa_supplicant.conf \
    wpa_cli \
    hostapd_cli

PRODUCT_PACKAGES += \
    libwifi-hal \
    libwifi-system \
    libwifikeystorehal \
    android.hardware.wifi@1.3 \
    android.hardware.wifi.supplicant@1.2

PRODUCT_PROPERTY_OVERRIDES += wifi.supplicant_scan_interval=15

# Bluetooth HAL
PRODUCT_PACKAGES += \
 android.hardware.bluetooth@1.0-service\
    android.hardware.bluetooth@1.0-impl \
    libbt-vendor
##  uncomment when we implement bluetooth audio hal
##  android.hardware.bluetooth.audio@2.0 \

# Override for providing bluetooth adress fallback and turning audio_hal (new in Android Q off)
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.bt.bdaddr_path=/sys/module/scsc_bt/parameters/bluetooth_address_fallback \
    persist.bluetooth.bluetooth_audio_hal.disabled=true

PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1 \
	debug.sf.disable_backpressure=1

# set lcd_density for 1080p hdmi monitors for e850-96 board
PRODUCT_PROPERTY_OVERRIDES += ro.sf.lcd_density=160

# adoptable storage
PRODUCT_PROPERTY_OVERRIDES += ro.crypto.volume.filenames_mode=aes-256-cts
#PRODUCT_PROPERTY_OVERRIDES += ro.crypto.allow_encrypt_override=true

# LMK
PRODUCT_PROPERTY_OVERRIDES += \
	ro.lmk.use_minfree_levels=true

# hw composer HAL
PRODUCT_PACKAGES += \
	hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME) \
    android.hardware.graphics.composer@2.2-impl \
    android.hardware.graphics.composer@2.2-service
    # vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
#PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
#PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi

# AI chip firmware
PRODUCT_COPY_FILES += \
	device/samsung/erd850/firmware/gti/gti_firmware_rev_12_dongle.img:$(TARGET_COPY_OUT_VENDOR)/firmware/gti_firmware_rev_12_dongle.img
PRODUCT_PACKAGES += \
	gti5801_fw_loader

####################################
## VIDEO
####################################
# MFC firmware
PRODUCT_COPY_FILES += \
	device/samsung/erd850/firmware/mfc_fw_v10.21.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin

PRODUCT_PACKAGES += \
   libstagefrighthw \
   libExynosOMX_Core \
   libExynosOMX_Resourcemanager \
   libOMX.Exynos.MPEG4.Decoder \
   libOMX.Exynos.AVC.Decoder \
   libOMX.Exynos.WMV.Decoder \
   libOMX.Exynos.VP8.Decoder \
   libOMX.Exynos.HEVC.Decoder \
   libOMX.Exynos.MPEG4.Encoder \
   libOMX.Exynos.AVC.Encoder \
   libOMX.Exynos.VP8.Encoder \
   libOMX.Exynos.HEVC.Encoder \

# mediacodec.policy
PRODUCT_COPY_FILES += \
	device/samsung/erd850/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy

# OpenMAX IL configuration files
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_audio.xml \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml \
	frameworks/av/media/libstagefright/data/media_codecs_google_video.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_video.xml \
	device/samsung/erd850/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml \
	device/samsung/erd850/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
	device/samsung/erd850/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml \
    device/samsung/erd850/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

# AVIExtractor
PRODUCT_PACKAGES += \
	libaviextractor

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

# Exynos OpenVX framework
# PRODUCT_PACKAGES += \
		libexynosvision

ifeq ($(TARGET_USES_CL_KERNEL),true)
PRODUCT_PACKAGES += \
       libopenvx-opencl
endif

#GPS
#PRODUCT_PACKAGES += \
#    android.hardware.gnss@2.0-impl \
#    vendor.samsung.hardware.gnss@1.0-impl \
#    vendor.samsung.hardware.gnss@1.0-service
#
#PRODUCT_PROPERTY_OVERRIDES += \
#    persist.vendor.gnsslog.maxfilesize=256 \
#    persist.vendor.gnsslog.status=0
##    exynos.gnss.path.log=/data/vendor/gps/

#Gatekeeper
PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

# Eden
PRODUCT_PACKAGES += \
       vendor.samsung_slsi.hardware.eden_runtime@1.0-impl \
       vendor.samsung_slsi.hardware.eden_runtime@1.0-service \
       android.hardware.neuralnetworks@1.2-service.eden-drv

PRODUCT_PROPERTY_OVERRIDES += \
       log.tag.EDEN=INFO \
       ro.eden.devices=CPU1_GPU1 \

# CryptoManger
PRODUCT_PACKAGES += \
	tlcmdrv \

# CryptoManager test app for eng build
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	tlcmtest \
	cm_test
endif

# Keymaster
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-impl \
	android.hardware.keymaster@4.0_tee-service \
	tlkeymasterM \
	keymaster_drv

# ArmNN driver
# PRODUCT_PACKAGES += \
  android.hardware.neuralnetworks@1.1-service-armnn

PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/12100000.dwmmc0/by-name/frp

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# vulkan version information
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml

# AVB2.0, to tell PackageManager that the system supports Verified Boot(PackageManager.FEATURE_VERIFIED_BOOT)
ifeq ($(BOARD_AVB_ENABLE), true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.verified_boot.xml:system/etc/permissions/android.software.verified_boot.xml
endif

# HIDL memtrack
PRODUCT_PACKAGES += \
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    memtrack.$(TARGET_BOARD_PLATFORM)

#VNDK
PRODUCT_PACKAGES += \
	vndk-libs

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

#VENDOR
PRODUCT_PACKAGES += \
	libGLES_mali \
	libGLES_mali32 \
	libOpenCL \
	libOpenCL32 \
	whitelist

PRODUCT_PACKAGES += \
	mfc_fw.bin

PRODUCT_PACKAGES += \
	exynos-thermald

$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)

## telephony.mk, relevant packages and properties will be defined in device-vendor.mk
#$(call inherit-product, vendor/samsung_slsi/telephony/common/device-vendor.mk)
##$(call inherit-product, hardware/samsung_slsi/exynos5/exynos5.mk)
$(call inherit-product-if-exists, hardware/samsung_slsi/exynos850/exynos850.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor_v2.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/exynos/eden/eden.mk)
##$(call inherit-product, device/samsung/erd850/telephony_binaries/telephony_binaries.mk)
## $(call inherit-product-if-exists, vendor/samsung_slsi/exynos/camera/hal3/camera.mk)
#$(call inherit-product, device/samsung/erd850/gnss_binaries/gnss_binaries.mk)

# Installs gsi keys into ramdisk, to boot a GSI with verified boot.
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

## MAKE mx file for WLBT ##
ifneq ($(findstring mx, $(wildcard vendor/samsung_slsi/mx140/firmware/mx)),)
PRODUCT_COPY_FILES += vendor/samsung_slsi/mx140/firmware/mx:vendor/etc/wifi/mx
endif

# top level makefile to call all necessary SCSC Wifi-BT makefiles
$(call inherit-product-if-exists, device/samsung/erd850/scsc_wlbt.mk)

## WLBT Logging ##
PRODUCT_PROPERTY_OVERRIDES += \
        persist.vendor.wlbtlog.maxfilesize=50 \
        persist.vendor.wlbtlog.maxfiles=5

$(call inherit-product, vendor/samsung_slsi/scsc_tools/wlbt/device-vendor.mk)

## IMSService ##
# IMSService build for both types (source build, prebulit apk/so build).
# Please make sure that only one of both ShannonIms(apk git) shannon-ims(src git) is present in the repo.
# This will be called only if IMSService is building with prebuilt binary for stable branches.
$(call inherit-product-if-exists, packages/apps/ShannonIms/device-vendor.mk)
# This will be called only if IMSService is building with source code for dev branches.
$(call inherit-product-if-exists, vendor/samsung_slsi/ims/shannon-ims/device-vendor.mk)#$(warning current dir: $(dir $(TARGET_BOARD_INFO_FILE)))

#mptool
#include device/samsung/erd850/mptool/device.mk
# include $(dir $(TARGET_BOARD_INFO_FILE))mptool/device.mk

# ifeq ($(BOARD_USE_FACTORY_IMAGES), true)
# BOARD_USE_SSCR_TOOLS += slogkit

# endif
# BOARD_USE_SSCR_TOOLS += mptool modem wcn
# $(call inherit-product-if-exists, vendor/samsung_slsi/sscr_tools/device.mk)

# NFC Feature
#$(call inherit-product-if-exists, device/samsung/erd850/nfc/device-nfc.mk)

PRODUCT_COPY_FILES += \
	device/samsung/erd850/exynos-thermal/exynos-thermal.conf:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.conf \
	device/samsung/erd850/exynos-thermal/exynos-thermal.env:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.env
