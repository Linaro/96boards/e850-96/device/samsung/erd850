#define LOG_TAG "gti-fw-loader"
#include <log/log.h>

#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define MAX_FSIZE	0x4004
#define I2C_SLAVE	0x0703
#define I2C_ADDRESS	0x60
#define FW_FILE		"/vendor/firmware/gti_firmware_rev_12_dongle.img"

enum {
	GET_PROD_ID	= 0x00000001,
	GET_FW_VERSION	= 0x00000002,
	GET_STATUS	= 0x00000003,
	FW_LOAD_START	= 0x00000004,
	FW_LOAD_END	= 0x00000008,
	FW_ROM_BOOT	= 0x00f42506,
};

static unsigned int mVersion;
static unsigned int mVID;
static unsigned int mDID;

unsigned int get_fw_value(int handle, int cmd, int rlen, unsigned char *buf)
{
	int ret;

	write(handle, (unsigned char*)&cmd, 4);

	ret = read(handle, buf, rlen);
	if (ret <= 0)
		ALOGE("Error: Failed to read cmd ret=%d\n", ret);

	return ret;
}

void load_5801_firmware_end(int handle)
{
	int cmd = FW_LOAD_END;

	write(handle, (unsigned char*)&cmd, 4);
	usleep(100000);
	cmd = FW_ROM_BOOT;
	write(handle, (unsigned char*)&cmd, 4);
}

void load_5801_firmware(int handle, unsigned char *data, int bytes)
{
	int cmd = FW_LOAD_START;

	write(handle, (unsigned char*)&cmd, 4);
	write(handle, data, bytes);
}

int main(int argc, char *argv[])
{
	int fd;
	unsigned char *data; /* byte */
	FILE *fp;
	unsigned int rdbuf[2];
	char filename[100];
	char i2c_node[20];

	if (argc != 2) {
		ALOGE("Error: Invalid arguments count\n");
		return EXIT_FAILURE;
	}

	sprintf(i2c_node, "%s%s", "/dev/i2c-", argv[1]);

	fd = open(i2c_node, O_RDWR);
	if (fd < 0) {
		ALOGE("Error: Can't open open %s: err = %d\n", i2c_node, errno);
		return EXIT_FAILURE;
	}

	/* Send slave address */
	if (ioctl(fd, I2C_SLAVE, I2C_ADDRESS) < 0) {
		ALOGE("Error: ioctl error: %d\n", errno);
		goto err_i2c_node;
	}

	/* ---- Download firmware ---- */
	strcpy(filename, FW_FILE);
	fp = fopen(filename, "r");
	if (fp == NULL) {
		ALOGE("Error: Failed to open file %s\n", filename);
		goto err_i2c_node;
	}

	data = (unsigned char*)malloc(258);
	if (data == NULL) {
		ALOGE("Error: Failed to allocate buffer\n");
		goto err_fw_file;
	}

	fseek(fp, 2, SEEK_SET);

	while (!feof(fp)) {
		int length = fread(data, sizeof(char), 258, fp);

		load_5801_firmware(fd, data, length);
	}

	load_5801_firmware_end(fd);
	fclose(fp);
	free(data);

	rdbuf[0] = 0;
	rdbuf[1] = 0;
	/* 8 bytes */
	if (get_fw_value(fd, GET_PROD_ID, 8, (unsigned char*)rdbuf) > 0) {
		mVID = rdbuf[0];
		mDID = rdbuf[1];
	} else {
		mVID = 0;
		mDID = 0;
	}
	rdbuf[0] = 0;

	/* Only 1 byte */
	if (get_fw_value(fd, GET_FW_VERSION, 1, (unsigned char*)rdbuf) > 0)
		mVersion = rdbuf[0];
	else
		mVersion = 0;

	close(fd);
	ALOGI("Success!");
	return EXIT_SUCCESS;

err_fw_file:
	fclose(fp);
err_i2c_node:
	close(fd);
	return EXIT_FAILURE;
}
